#!/usr/bin/env node


import chalk from 'chalk';
import figlet from 'figlet'
const inquirer = require('inquirer')
const shell = require('shelljs')
import * as cmdBashTerminal from 'child_process';
import fs from 'fs'
const clear = require('clear');
const program = require('commander');
import { criarPrimeNG } from './PrimeNGAngular/primeNGCreateFile';
import { criarNebular } from './NebularAngular/nebularCreateFile';
import { Subject } from 'rxjs';
import { config } from './config';
import ora from 'ora';
clear();
console.log(
  chalk.magenta(
    figlet.textSync('Ana-cli', {
      font: "Ghost",
      horizontalLayout: 'full'
    })
  )
);




program.version('0.0.10', '-v, --version')
program.description("O CLI para quem é preguiçoso como eu e não ta afim de programar. Estamos na versão 0.0.10 " + `\\o/` + ' Hooray!')
program.option('-t, --test', 'testes')
program.option('-p, --primeng', 'criar angular com o PrimeNG ja instalado, pendente a configuração do mesmo')
program.option('-m, --mdb', 'criar angular com o MDB ja instalado, pendente a configuração do mesmo')
program.option('-n, --nebular', 'criar angular com o Nebular ja instalado, pendente a configuração do mesmo')
program.option('--dbjson', 'Incia um dbJson(caso esteja instalado na maquina) usando o arquivo db.json que exista na pasta em ')
program.parse(process.argv);



if (program.test) {
  console.log(chalk.blueBright('Iniciando Teste angular'));

  const comando = 'ng new kaleido --style=scss --routing'
  console.log(chalk.blueBright('Pensando bem...'))

  /*   cmdBashTerminal.exec(comando, { timeout: 600000 }, ((error: any, stdout: any, stderr: any) => {
      console.log(comando);
      console.log(chalk.redBright(error));
      console.log(stdout)
    })
    ) */
}

if (program.dbjson) {
  const comando = 'json-server --watch db.json'
  cmdBashTerminal.exec(comando, ((error: any, stdout: any, stderr: any) => {
    if (error) {
      console.log(chalk.red(error));
    } else {
      console.log(stdout);
    }
  })
  )
}

if (program.primeng) {
  criarPrimeNG();
}

if (program.nebular) {
  criarNebular()
}