export const NebularModule = `
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbCardModule,
  NbLayoutModule,
  NbMenuModule,
  NbSidebarService,
  NbSelectModule,
  NbRouteTabsetModule,
  NbSearchModule,
  NbSidebarModule,
  NbTabsetModule,
  NbThemeModule,
  NbUserModule,
  NbButtonModule,
  NbCalendarModule,
  NbAlertModule,
  NbCheckboxModule,
  NbAccordionModule,
  NbStepperModule,
  NbInputModule,
  NbToastrModule,
  NbWindowModule,
  NbDialogModule,
  NbProgressBarModule
} from '@nebular/theme';





const NB_THEME_PROVIDERS = [
  ...NbThemeModule.forRoot(
    {
      name: 'cosmic',
    },
  ).providers,
  ...NbSidebarModule.forRoot().providers,
  ...NbMenuModule.forRoot().providers,
  ...NbDialogModule.forRoot().providers,
  ...NbWindowModule.forRoot().providers,
  ...NbToastrModule.forRoot().providers,

];

@NgModule({
  imports: [
    CommonModule,
    NbActionsModule,
    NbCardModule,
    NbLayoutModule,
    NbMenuModule,
    NbSelectModule,
    NbRouteTabsetModule,
    NbSearchModule,
    NbSidebarModule,
    NbTabsetModule,
    NbThemeModule,
    NbUserModule,
    NbButtonModule,
    NbCalendarModule,
    NbAlertModule,
    NbCheckboxModule,
    NbAccordionModule,
    NbStepperModule,
    NbInputModule,
    NbToastrModule,
    NbWindowModule,
    NbDialogModule,
    NbProgressBarModule
  ],
  declarations: [],
  exports: [
    NbActionsModule,
    NbCardModule,
    NbLayoutModule,
    NbMenuModule,
    NbSelectModule,
    NbRouteTabsetModule,
    NbSearchModule,
    NbSidebarModule,
    NbTabsetModule,
    NbThemeModule,
    NbUserModule,
    NbButtonModule,
    NbCalendarModule,
    NbAlertModule,
    NbCheckboxModule,
    NbAccordionModule,
    NbStepperModule,
    NbInputModule,
    NbToastrModule,
    NbWindowModule,
    NbDialogModule,
    NbProgressBarModule
  ],
  providers: []
})
export class NebularModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: NebularModule,
      providers: [...NB_THEME_PROVIDERS],
    };
  }
}

`