

import * as inquirer from 'inquirer'



export interface inquirer {
  type: 'input' | 'number' | 'confirm' | 'list' | 'rawlist' | 'expand' | 'checkbox' | 'password' | 'editor'
  name: string;
  message: string | Function;
  default: string | number | boolean | Array<any> | Function;
  choices?: Array<any> | Function;
  validate?: Function;
  filter?: Function
  transformer?: Function;
  when?: Function | Boolean;
  pageSize?: number;
  prefix?: string;
  suffix?: string;
}


export let questions: inquirer[]
questions = [
  {
    type: 'input',
    name: 'AngularProjectName',
    message: 'Qual será o nome do Projeto?',
    default: 'kaleido',
    filter: ((res: string) => {
      return res.toLowerCase()
    })
  }
]