import chalk from 'chalk';
const inquirer = require('inquirer')
import * as cmdBashTerminal from 'child_process';
import { Subject, timer, interval } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import ora from 'ora';
import { config } from './../config';
import figlet from 'figlet'
import fs from 'fs'
import { questions } from './nebularquestions';
let contador: any = 0;
interface stbjectT {
  comando: string;
  nomeLocal: string;
  nomeDoApp: string;
  dataTransitorio: any;
  contador: string;
}
let temporizador: number = 0
const finalizarTimer = new Subject()
const rodarNPM = new Subject<stbjectT>();
const spinner = ora(config.Processo.CriandoAngular)
export const criarNebular = (() => {

  console.log('Okay, o que temos aqui hein senhores?');
  console.log('YES! Nebular ai vamos nós =>');
  inquirer.prompt(questions).then((result: any) => {
    console.log(result);
    console.log(chalk.magenta('Olá. Estou inicializando seu aplicativo agora'));
    spinner.start();
    console.log(chalk.blueBright(''));
    console.log(chalk.blueBright('AVISO! WARNING! HEY LINK, LISTEN!'));
    console.log(chalk.blueBright('AVISO! WARNING! HEY LINK, LISTEN!'));
    console.log(chalk.blueBright('AVISO! WARNING! HEY LINK, LISTEN!'));
    console.log(chalk.blueBright('AVISO! WARNING! HEY LINK, LISTEN!'));
    console.log(chalk.blueBright('Iniciando criação do Nebular'));
    let comando = 'ng new ' + result.AngularProjectName + ' --style=scss --routing'
    let res: stbjectT = {
      comando: comando,
      nomeLocal: './' + result.AngularProjectName + '',
      nomeDoApp: result.AngularProjectName,
      dataTransitorio: null,
      contador: config.PrimeNGProcess.CriarAngular
    }
    rodarNPM.next(res)
  })
})
rodarNPM.asObservable().subscribe((res) => {
  const tempo = timer(2000, 1000).pipe(
    takeUntil(finalizarTimer))
    .subscribe((deltaT) => {
      temporizador = deltaT
      if (temporizador === 20) {
        console.log(chalk.yellow('Eu sei que ta demorando, mas espera só mais um pouquinho'));
      }
      if (temporizador === 40) {
        console.log(chalk.yellow('Okay, talvez eu esteja enrolando um pouco. Vou tentar acelar mais as coisas'));
      }
      if (temporizador === 60) {
        console.log(chalk.yellow('1 MINUTO! EBAAAaaa... é...eu sei'));
      }
      if (temporizador === 120) {
        console.log(chalk.yellow('Em minha defesa eu...peço desculpas 😔'));
      }
    });
  switch (res.contador) {
    case config.NebularProcess.CriarAngular:
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Criando angularApp'));
      cmdBashTerminal.exec(res.comando, ((error: any, stdout: any, stderr: any) => {
        spinner.stop();
        if (error !== null) {
          console.log(chalk.red('Error', error));
        }
        temporizador = 0;
        console.log(chalk.green('Tudo funcionando'));
        console.log(chalk.green('Angular Criado. Configurando Ambiente PrimeNG'))
        spinner.start()
        res.contador = config.NebularProcess.RodarNpm
        rodarNPM.next(res)
      }))
      break;
    case config.NebularProcess.RodarNpm:
      res.comando = 'cd ' + res.nomeLocal + config.NPMInstances.NebularProcess;
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Instalando pacotes NPM'));
      cmdBashTerminal.exec(res.comando, ((error: any, stdout: any, stderr: any) => {
        spinner.stop();
        console.log(stdout)
        res.comando = '';
        temporizador = 0;
        res.contador = config.NebularProcess.CriarNebularModule;
        console.log(chalk.green('Nebular instalado. Vou criar um Modulo com alguns imports do Nebular para você.😊'))
        console.log(chalk.green('Se precisar de mais componentes Nebular, só ir no site da Akveo. coloco ele no final do modulo pra você'))
        rodarNPM.next(res)
      }))
      break;
    case config.NebularProcess.CriarNebularModule:
      res.comando = res.nomeLocal + '/src/nebular.module.ts';
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Criando Modulo do Nebular'));
      fs.writeFile(res.comando, config.Arquivos.Nebular.NebularModule, ((err: any) => {
        if (err !== null) {
          console.log(chalk.red('Error', err));
        }
        temporizador = 0;
        res.contador = config.NebularProcess.ModificarAppModule;
        console.log(chalk.green('NebularModule.ts criado😊') + ' inserindo no app.module.ts')
        rodarNPM.next(res)
      }))
      break;

    case config.NebularProcess.ModificarAppModule:
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Modificando o app.module.ts'));
      res.comando = res.nomeLocal + '/src/app/app.module.ts';
      fs.writeFile(res.comando, config.Arquivos.Nebular.AppModuleModificado, ((err: any) => {
        if (err !== null) {
          console.log(chalk.red('Error', err));
        }
        temporizador = 0;
        res.contador = config.NebularProcess.LerAngularJson;
        console.log(chalk.green('app.module.ts modificado😊') + ' Modificando AngularJson')
        rodarNPM.next(res)
      }))
      break;
    case config.NebularProcess.LerAngularJson:
      res.comando = res.nomeLocal + '/angular.json';
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Lendo Angular.json'));
      fs.readFile(res.comando, ((error, data: any) => {
        const resultado = JSON.parse(data)
        const posicao = res.nomeDoApp
        console.log(chalk.red('Modificando o Angular.json'));
        resultado.projects[posicao].architect.build.options.styles.push('src/PrimeFlex.scss');
        resultado.projects[posicao].architect.build.options.styles.push('node_modules/@nebular/theme/styles/prebuilt/cosmic.css');
        resultado.projects[posicao].architect.build.options.styles.push('node_modules/nebular-icons/scss/nebular-icons.scss');
        resultado.projects[posicao].architect.build.options.styles.push('node_modules/eva-icons/style/eva-icons.css');
        resultado.projects[posicao].architect.build.options.styles.push('node_modules/socicon/css/socicon.css');
        temporizador = 0;
        config.Arquivos.Nebular.AngularJson = JSON.stringify(resultado);
        res.contador = config.PrimeNGProcess.ModificarAngularJson
        rodarNPM.next(res)
      }))

      break;

    case config.NebularProcess.ModificarAngularJson:
      res.comando = res.nomeLocal + '/angular.json';
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Salvando o novo Angular.json'));
      fs.writeFile(res.comando, config.Arquivos.Nebular.AngularJson, ((err: any) => {
        if (err !== null) {
          console.log(chalk.red('Error', err));
        }
        temporizador = 0;
        res.contador = config.NebularProcess.criarPrimeFlex
        console.log(chalk.green('angular.Json modificado😊') + ' criando PrimeFlex')
        rodarNPM.next(res)
      }))
      break;
    case config.NebularProcess.criarPrimeFlex:
      res.comando = res.nomeLocal + '/src/primeFlex.scss';
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Criando o PrimeFlex pra ajustar no layout da Pagina'));
      fs.writeFile(res.comando, config.Arquivos.Nebular.PrimeFlex, ((err: any) => {
        if (err !== null) {
          console.log(chalk.red('Error', err));
        }
        temporizador = 0;
        tempo.unsubscribe()
        finalizarTimer.next()
        res.comando = 'cd ' + res.nomeLocal + '&& code .';
        cmdBashTerminal.exec(res.comando, ((error: any, stdout: any, stderr: any) => {
          rodarNPM.complete()
          console.log(chalk.green('E pronto😊. Boa sorte'))
        }))
      }))
      break;
  }
})






// npm publish --registry http://10.2.1.127:4873
