import chalk from 'chalk';
const inquirer = require('inquirer')
import * as cmdBashTerminal from 'child_process';
import { questions } from "./primeNGquestions";
import { Subject, timer, interval } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import ora from 'ora';
import { config } from './../config';
import figlet from 'figlet'
import fs from 'fs'
let contador: any = 0;
interface stbjectT {
  comando: string;
  nomeLocal: string;
  nomeDoApp: string;
  dataTransitorio: any;
  contador: string;
}
let temporizador: number = 0
const finalizarTimer = new Subject()
const rodarNPM = new Subject<stbjectT>();
const spinner = ora(config.Processo.CriandoAngular)

export const criarPrimeNG = (() => {
  console.log('Okay, o que temos aqui hein senhores?');
  console.log('PrimeNG? Bem...okay então. ai vamos nós =>');
  inquirer.prompt(questions).then((result: any) => {
    console.log(result);
    console.log(chalk.magenta('Olá. Estou inicializando seu aplicativo agora'));
    spinner.start();
    console.log(chalk.redBright(''));
    console.log(chalk.redBright('AVISO! WARNING! HEY LINK, LISTEN!'));
    console.log(chalk.redBright('AVISO! WARNING! HEY LINK, LISTEN!'));
    console.log(chalk.redBright('AVISO! WARNING! HEY LINK, LISTEN!'));
    console.log(chalk.redBright('AVISO! WARNING! HEY LINK, LISTEN!'));
    console.log(chalk.redBright('Iniciando criação do Aplicativo'));
    let comando = 'ng new ' + result.AngularProjectName + ' --style=scss --routing'
    let res: stbjectT = {
      comando: comando,
      nomeLocal: './' + result.AngularProjectName + '',
      nomeDoApp: result.AngularProjectName,
      dataTransitorio: null,
      contador: config.PrimeNGProcess.CriarAngular
    }
    rodarNPM.next(res)
  })
})

rodarNPM.asObservable().subscribe((res) => {
  const tempo = timer(2000, 1000).pipe(
    takeUntil(finalizarTimer))
    .subscribe((deltaT) => {
      temporizador = deltaT
      if (temporizador === 20) {
        console.log(chalk.yellow('Eu sei que ta demorando, mas espera só mais um pouquinho'));
      }
      if (temporizador === 40) {
        console.log(chalk.yellow('Okay, talvez eu esteja enrolando um pouco. Vou tentar acelar mais as coisas'));
      }
      if (temporizador === 60) {
        console.log(chalk.yellow('1 MINUTO! EBAAAaaa... é...eu sei'));
      }
      if (temporizador === 120) {
        console.log(chalk.yellow('Em minha defesa eu...peço desculpas 😔'));
      }
    });


  switch (res.contador) {
    case config.PrimeNGProcess.CriarAngular:
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Criando angularApp'));
      cmdBashTerminal.exec(res.comando, ((error: any, stdout: any, stderr: any) => {
        spinner.stop();
        if (error !== null) {
          rodarNPM.complete();
          spinner.stop();
          tempo.unsubscribe();
          console.log(chalk.red('Error', error));
        }
        temporizador = 0;
        console.log(chalk.green('Tudo funcionando'));
        console.log(chalk.green('Angular Criado. Configurando Ambiente PrimeNG'));
        spinner.start();
        res.contador = config.PrimeNGProcess.RodarNpm;
        rodarNPM.next(res);
      }))
      break;
    case config.PrimeNGProcess.RodarNpm:
      res.comando = 'cd ' + res.nomeLocal + config.NPMInstances.PrimeNGProcess;
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Instalando pacotes NPM'));
      cmdBashTerminal.exec(res.comando, ((error: any, stdout: any, stderr: any) => {
        spinner.stop();
        console.log(stdout)
        res.comando = ''
        temporizador = 0;
        res.contador = config.PrimeNGProcess.CriarPrimeModule;
        console.log(chalk.green('PrimeNg instalado. Vou criar um Modulo com alguns imports do PrimeNG para você. Só importar esse modulo no App.module.ts 😊'))
        rodarNPM.next(res)
      }))
      break;
    case config.PrimeNGProcess.CriarPrimeModule:
      res.comando = res.nomeLocal + '/src/primeNG.module.ts'
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Criando Modulo do PrimeNG'));
      fs.writeFile(res.comando, config.Arquivos.PrimeNG.PrimeModule, ((err: any) => {
        if (err !== null) {
          rodarNPM.complete();
          console.log(chalk.red('Error', err));
        }
        temporizador = 0;
        res.contador = config.PrimeNGProcess.ModificarAppModule;
        console.log(chalk.green('PrimeNgModule.ts criado😊') + ' inserindo no app.module.ts')
        rodarNPM.next(res)
      }))
      break;
    case config.PrimeNGProcess.ModificarAppModule:
      res.comando = res.nomeLocal + '/src/app/app.module.ts'
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Modificando o app.module.ts'));
      fs.writeFile(res.comando, config.Arquivos.PrimeNG.AppModuleModificado, ((err: any) => {
        if (err !== null) {
          rodarNPM.complete();
          console.log(chalk.red('Error', err));
        };
        temporizador = 0;
        res.contador = config.PrimeNGProcess.LerAngularJson;
        console.log(chalk.green('app.module.ts modificado😊') + ' Modificando AngularJson')
        rodarNPM.next(res)
      }))
      break;
    case config.PrimeNGProcess.LerAngularJson:
      res.comando = res.nomeLocal + '/angular.json';
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Lendo Angular.json'));
      fs.readFile(res.comando, ((error, data: any) => {
        const resultado = JSON.parse(data)
        const posicao = res.nomeDoApp
        console.log(chalk.red('Modificando o Angular.json'));
        resultado.projects[posicao].architect.build.options.styles.push('src/PrimeFlex.scss');
        resultado.projects[posicao].architect.build.options.styles.push('node_modules/primeng/resources/themes/nova-light/theme.css');
        resultado.projects[posicao].architect.build.options.styles.push('node_modules/primeng/resources/primeng.min.css');
        resultado.projects[posicao].architect.build.options.styles.push('node_modules/primeicons/primeicons.css');
        temporizador = 0;
        config.Arquivos.PrimeNG.AngularJson = JSON.stringify(resultado);
        res.contador = config.PrimeNGProcess.ModificarAngularJson
        rodarNPM.next(res)
      }))
      break;
    case config.PrimeNGProcess.ModificarAngularJson:
      res.comando = res.nomeLocal + '/angular.json'
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Salvando o novo Angular.json'));
      fs.writeFile(res.comando, config.Arquivos.PrimeNG.AngularJson, ((err: any) => {
        if (err !== null) {
          rodarNPM.complete();
          console.log(chalk.red('Error', err));
        };
        temporizador = 0;
        res.contador = config.PrimeNGProcess.criarPrimeFlex
        console.log(chalk.green('angular.Json modificado😊') + ' criando PrimeFlex')
        rodarNPM.next(res)
      }))
      break;
    case config.PrimeNGProcess.criarPrimeFlex:
      res.comando = res.nomeLocal + '/src/primeFlex.scss'
      console.log(chalk.red('comando a ser executado:', res.comando));
      console.log(chalk.red('Criando o PrimeFlex pra ajustar no layout da Pagina'));
      fs.writeFile(res.comando, config.Arquivos.PrimeNG.PrimeFlex, ((err: any) => {
        if (err !== null) {
          rodarNPM.complete();
          console.log(chalk.red('Error', err));
        }
        temporizador = 0;
        tempo.unsubscribe()
        finalizarTimer.next()
        console.log(chalk.green('E pronto😊. Boa sorte'))
        rodarNPM.complete()
      }))
      break;
    default:
      break;
  }


})

