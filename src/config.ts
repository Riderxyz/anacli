
//Comum a todos
import { PrimeFlex } from './CommonFiles/primeFlex';

//PrimeNG
import { PrimeModule } from './PrimeNGAngular/Sample/primeNGModule';
import { AppModulePrimeNG } from './PrimeNGAngular/Sample/AppModule';
import { AngularJsonPrimeNG } from './PrimeNGAngular/Sample/AngularJson';

//Nebular
import { NebularModule } from './NebularAngular/Sample/nebularModule';
import { AngularJsonNebular } from './NebularAngular/Sample/AngularJsonNebular';
import { AppModuleNebular } from './NebularAngular/Sample/AppModuleNebular';


export const config = {

  Processo: {
    CriandoAngular: 'Criando seu Aplicativo agora. Devo demorar um pouquinho',
    ErroAoCriarAngular: 'Opa, parece que deu um erro. Bem, a culpa não é minha Hunf',
  },
  Arquivos: {
    PrimeNG: {
      PrimeModule: PrimeModule,
      AppModuleModificado: AppModulePrimeNG,
      AngularJson: AngularJsonPrimeNG,
      PrimeFlex: PrimeFlex,
    },
    Nebular: {
      NebularModule: NebularModule,
      AppModuleModificado: AppModuleNebular,
      AngularJson: AngularJsonNebular,
      PrimeFlex: PrimeFlex,
    }
  },
  NPMInstances: {
    PrimeNGProcess: ' && npm i primeng primeicons @angular/animations @angular/cdk --save',
    NebularProcess: '&& npm i @angular/cdk @angular/animations bootstrap @nebular/auth @nebular/bootstrap @nebular/security @nebular/theme @angular/animations eva-icons nebular-icons socicon ionicons ng2-completer'
  },
  PrimeNGProcess: {
    CriarAngular: 'criarAngular',
    RodarNpm: 'rodarNPM',
    CriarPrimeModule: 'criarPrimeNGModule',
    ModificarAppModule: 'modificarAppModule',
    LerAngularJson: 'lerAngularJson',
    ModificarAngularJson: 'modificarAngularJson',
    criarPrimeFlex: 'criarPrimeFlex'
  },
  NebularProcess: {
    CriarAngular: 'criarAngular',
    RodarNpm: 'rodarNPM',
    CriarNebularModule: 'criarNebularModule',
    ModificarAppModule: 'modificarAppModule',
    LerAngularJson: 'lerAngularJson',
    ModificarAngularJson: 'modificarAngularJson',
    criarPrimeFlex: 'criarPrimeFlex'
  }
}