# Ana
## O CLI para preguiçosos como eu.

Ah! Antes de começar, Ana não é um anagrama nem uma Sigla. Escolhi o nome Ana por que ele é um Palíndromo. O que é isso? Leia Ana ao contrario. Isso mesmo. Continua a mesma coisa, e isso que significa um palíndromo.

Aqui vou usar esta parte para fazer uma especie de CHANGELOG. Se vai ficar bonito? Claro...que não ¬¬ Eu sou um iniciante nessas coisas, o que esperava?
Anyway, aí vamos nós:

### 0.0.8 (24/04/2019)
* dbJson adicionadoaos processos.
* Nebular ainda em 90%

### 0.0.7 (12/03/2019)

* Processo do Nebular esta em 90%
* Processo do PrimeNg usa agr os arquivos do config.ts
  
### 0.0.6 (12/03/2019)

* Processo do Nebular ainda em 30%
* Config.ts crescendo. O plano é que ele fique com toda a parte de strings e constantes do app
* Arquivos que são comuns a todos os Geradores serão movidos para uma pasta de comum acesso

### 0.0.5 (06/03/2019)

* Processo do PrimeNG esta finalizado. 100%

### 0.0.4 (04/03/2019)

* Acerto quando a criação do Modulo do PrimeNG e quanto ao RxJs
* Criar um AngularAPP com o PrimeNG agr é totalmente possivel e facil.
* Usando switch para cuidar das diversas funções, como criar arquivos, executar child_process e etcs.
* Ainda pendente uma maneira de alterar o angularJson. Acredito que usando o Json.Parse seja possivel
* Proximo update, o app.module.ts da aplicação ja virá com o primeNG.module ja instanciado

### 0.0.3 (01/03/2019)

* Separação de funções. PrimeNG ja esta separado.
* Utilizando o RxJs para chamar as funções do child_process e assim econimizar codigo.
* Ainda com erro na chamada e na criação do modulo do PrimeNG



### 0.0.2 (26/02/2019)

* Não usando mais o shelljs. Decidi trocar para o child_process que tem uma interface ja nativa do Node(se eu soubesse disso antes...)
* Tipando o que posso, e me preparando para usar outros arquivos como rotas para as opções. Acho q ficará mais organizado se for desse jeito. Talvez até crie um Config.ts par coisas mais importantes. 
* A pasta lixo2 serve até agr como um local perfeito para testes dos projetos angular. Vou criar depois um comando para limpar ela automaticamente assim que a Ana começar a rodar


Ana é um CLI pessoal que serve para criar aplicações angular com 3 pacotes de ui ja pré indicados:
* Material Bootstrap Design(MDB)
* PrimeNG
* Nebular

Ou então com pacotes que o usuario poderá indicar. Quantos o mesmo quiser.

