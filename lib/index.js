#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = __importDefault(require("chalk"));
var figlet_1 = __importDefault(require("figlet"));
var inquirer = require('inquirer');
var shell = require('shelljs');
var cmdBashTerminal = __importStar(require("child_process"));
var clear = require('clear');
var program = require('commander');
var primeNGCreateFile_1 = require("./PrimeNGAngular/primeNGCreateFile");
var nebularCreateFile_1 = require("./NebularAngular/nebularCreateFile");
clear();
console.log(chalk_1.default.magenta(figlet_1.default.textSync('Ana-cli', {
    font: "Ghost",
    horizontalLayout: 'full'
})));
program.version('0.0.10', '-v, --version');
program.description("O CLI para quem é preguiçoso como eu e não ta afim de programar. Estamos na versão 0.0.10 " + "\\o/" + ' Hooray!');
program.option('-t, --test', 'testes');
program.option('-p, --primeng', 'criar angular com o PrimeNG ja instalado, pendente a configuração do mesmo');
program.option('-m, --mdb', 'criar angular com o MDB ja instalado, pendente a configuração do mesmo');
program.option('-n, --nebular', 'criar angular com o Nebular ja instalado, pendente a configuração do mesmo');
program.option('--dbjson', 'Incia um dbJson(caso esteja instalado na maquina) usando o arquivo db.json que exista na pasta em ');
program.parse(process.argv);
if (program.test) {
    console.log(chalk_1.default.blueBright('Iniciando Teste angular'));
    var comando = 'ng new kaleido --style=scss --routing';
    console.log(chalk_1.default.blueBright('Pensando bem...'));
    /*   cmdBashTerminal.exec(comando, { timeout: 600000 }, ((error: any, stdout: any, stderr: any) => {
        console.log(comando);
        console.log(chalk.redBright(error));
        console.log(stdout)
      })
      ) */
}
if (program.dbjson) {
    var comando = 'json-server --watch db.json';
    cmdBashTerminal.exec(comando, (function (error, stdout, stderr) {
        if (error) {
            console.log(chalk_1.default.red(error));
        }
        else {
            console.log(stdout);
        }
    }));
}
if (program.primeng) {
    primeNGCreateFile_1.criarPrimeNG();
}
if (program.nebular) {
    nebularCreateFile_1.criarNebular();
}
