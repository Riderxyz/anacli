"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//Comum a todos
var primeFlex_1 = require("./CommonFiles/primeFlex");
//PrimeNG
var primeNGModule_1 = require("./PrimeNGAngular/Sample/primeNGModule");
var AppModule_1 = require("./PrimeNGAngular/Sample/AppModule");
var AngularJson_1 = require("./PrimeNGAngular/Sample/AngularJson");
//Nebular
var nebularModule_1 = require("./NebularAngular/Sample/nebularModule");
var AngularJsonNebular_1 = require("./NebularAngular/Sample/AngularJsonNebular");
var AppModuleNebular_1 = require("./NebularAngular/Sample/AppModuleNebular");
exports.config = {
    Processo: {
        CriandoAngular: 'Criando seu Aplicativo agora. Devo demorar um pouquinho',
        ErroAoCriarAngular: 'Opa, parece que deu um erro. Bem, a culpa não é minha Hunf',
    },
    Arquivos: {
        PrimeNG: {
            PrimeModule: primeNGModule_1.PrimeModule,
            AppModuleModificado: AppModule_1.AppModulePrimeNG,
            AngularJson: AngularJson_1.AngularJsonPrimeNG,
            PrimeFlex: primeFlex_1.PrimeFlex,
        },
        Nebular: {
            NebularModule: nebularModule_1.NebularModule,
            AppModuleModificado: AppModuleNebular_1.AppModuleNebular,
            AngularJson: AngularJsonNebular_1.AngularJsonNebular,
            PrimeFlex: primeFlex_1.PrimeFlex,
        }
    },
    NPMInstances: {
        PrimeNGProcess: ' && npm i primeng primeicons @angular/animations @angular/cdk --save',
        NebularProcess: '&& npm i @angular/cdk @angular/animations bootstrap @nebular/auth @nebular/bootstrap @nebular/security @nebular/theme @angular/animations eva-icons nebular-icons socicon ionicons ng2-completer'
    },
    PrimeNGProcess: {
        CriarAngular: 'criarAngular',
        RodarNpm: 'rodarNPM',
        CriarPrimeModule: 'criarPrimeNGModule',
        ModificarAppModule: 'modificarAppModule',
        LerAngularJson: 'lerAngularJson',
        ModificarAngularJson: 'modificarAngularJson',
        criarPrimeFlex: 'criarPrimeFlex'
    },
    NebularProcess: {
        CriarAngular: 'criarAngular',
        RodarNpm: 'rodarNPM',
        CriarNebularModule: 'criarNebularModule',
        ModificarAppModule: 'modificarAppModule',
        LerAngularJson: 'lerAngularJson',
        ModificarAngularJson: 'modificarAngularJson',
        criarPrimeFlex: 'criarPrimeFlex'
    }
};
