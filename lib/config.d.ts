export declare const config: {
    Processo: {
        CriandoAngular: string;
        ErroAoCriarAngular: string;
    };
    Arquivos: {
        PrimeNG: {
            PrimeModule: string;
            AppModuleModificado: string;
            AngularJson: string;
            PrimeFlex: string;
        };
        Nebular: {
            NebularModule: string;
            AppModuleModificado: string;
            AngularJson: string;
            PrimeFlex: string;
        };
    };
    NPMInstances: {
        PrimeNGProcess: string;
        NebularProcess: string;
    };
    PrimeNGProcess: {
        CriarAngular: string;
        RodarNpm: string;
        CriarPrimeModule: string;
        ModificarAppModule: string;
        LerAngularJson: string;
        ModificarAngularJson: string;
        criarPrimeFlex: string;
    };
    NebularProcess: {
        CriarAngular: string;
        RodarNpm: string;
        CriarNebularModule: string;
        ModificarAppModule: string;
        LerAngularJson: string;
        ModificarAngularJson: string;
        criarPrimeFlex: string;
    };
};
