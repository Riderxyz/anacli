"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = __importDefault(require("chalk"));
var inquirer = require('inquirer');
var cmdBashTerminal = __importStar(require("child_process"));
var primeNGquestions_1 = require("./primeNGquestions");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var ora_1 = __importDefault(require("ora"));
var config_1 = require("./../config");
var fs_1 = __importDefault(require("fs"));
var contador = 0;
var temporizador = 0;
var finalizarTimer = new rxjs_1.Subject();
var rodarNPM = new rxjs_1.Subject();
var spinner = ora_1.default(config_1.config.Processo.CriandoAngular);
exports.criarPrimeNG = (function () {
    console.log('Okay, o que temos aqui hein senhores?');
    console.log('PrimeNG? Bem...okay então. ai vamos nós =>');
    inquirer.prompt(primeNGquestions_1.questions).then(function (result) {
        console.log(result);
        console.log(chalk_1.default.magenta('Olá. Estou inicializando seu aplicativo agora'));
        spinner.start();
        console.log(chalk_1.default.redBright(''));
        console.log(chalk_1.default.redBright('AVISO! WARNING! HEY LINK, LISTEN!'));
        console.log(chalk_1.default.redBright('AVISO! WARNING! HEY LINK, LISTEN!'));
        console.log(chalk_1.default.redBright('AVISO! WARNING! HEY LINK, LISTEN!'));
        console.log(chalk_1.default.redBright('AVISO! WARNING! HEY LINK, LISTEN!'));
        console.log(chalk_1.default.redBright('Iniciando criação do Aplicativo'));
        var comando = 'ng new ' + result.AngularProjectName + ' --style=scss --routing';
        var res = {
            comando: comando,
            nomeLocal: './' + result.AngularProjectName + '',
            nomeDoApp: result.AngularProjectName,
            dataTransitorio: null,
            contador: config_1.config.PrimeNGProcess.CriarAngular
        };
        rodarNPM.next(res);
    });
});
rodarNPM.asObservable().subscribe(function (res) {
    var tempo = rxjs_1.timer(2000, 1000).pipe(operators_1.takeUntil(finalizarTimer))
        .subscribe(function (deltaT) {
        temporizador = deltaT;
        if (temporizador === 20) {
            console.log(chalk_1.default.yellow('Eu sei que ta demorando, mas espera só mais um pouquinho'));
        }
        if (temporizador === 40) {
            console.log(chalk_1.default.yellow('Okay, talvez eu esteja enrolando um pouco. Vou tentar acelar mais as coisas'));
        }
        if (temporizador === 60) {
            console.log(chalk_1.default.yellow('1 MINUTO! EBAAAaaa... é...eu sei'));
        }
        if (temporizador === 120) {
            console.log(chalk_1.default.yellow('Em minha defesa eu...peço desculpas 😔'));
        }
    });
    switch (res.contador) {
        case config_1.config.PrimeNGProcess.CriarAngular:
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Criando angularApp'));
            cmdBashTerminal.exec(res.comando, (function (error, stdout, stderr) {
                spinner.stop();
                if (error !== null) {
                    rodarNPM.complete();
                    spinner.stop();
                    tempo.unsubscribe();
                    console.log(chalk_1.default.red('Error', error));
                }
                temporizador = 0;
                console.log(chalk_1.default.green('Tudo funcionando'));
                console.log(chalk_1.default.green('Angular Criado. Configurando Ambiente PrimeNG'));
                spinner.start();
                res.contador = config_1.config.PrimeNGProcess.RodarNpm;
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.PrimeNGProcess.RodarNpm:
            res.comando = 'cd ' + res.nomeLocal + config_1.config.NPMInstances.PrimeNGProcess;
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Instalando pacotes NPM'));
            cmdBashTerminal.exec(res.comando, (function (error, stdout, stderr) {
                spinner.stop();
                console.log(stdout);
                res.comando = '';
                temporizador = 0;
                res.contador = config_1.config.PrimeNGProcess.CriarPrimeModule;
                console.log(chalk_1.default.green('PrimeNg instalado. Vou criar um Modulo com alguns imports do PrimeNG para você. Só importar esse modulo no App.module.ts 😊'));
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.PrimeNGProcess.CriarPrimeModule:
            res.comando = res.nomeLocal + '/src/primeNG.module.ts';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Criando Modulo do PrimeNG'));
            fs_1.default.writeFile(res.comando, config_1.config.Arquivos.PrimeNG.PrimeModule, (function (err) {
                if (err !== null) {
                    rodarNPM.complete();
                    console.log(chalk_1.default.red('Error', err));
                }
                temporizador = 0;
                res.contador = config_1.config.PrimeNGProcess.ModificarAppModule;
                console.log(chalk_1.default.green('PrimeNgModule.ts criado😊') + ' inserindo no app.module.ts');
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.PrimeNGProcess.ModificarAppModule:
            res.comando = res.nomeLocal + '/src/app/app.module.ts';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Modificando o app.module.ts'));
            fs_1.default.writeFile(res.comando, config_1.config.Arquivos.PrimeNG.AppModuleModificado, (function (err) {
                if (err !== null) {
                    rodarNPM.complete();
                    console.log(chalk_1.default.red('Error', err));
                }
                ;
                temporizador = 0;
                res.contador = config_1.config.PrimeNGProcess.LerAngularJson;
                console.log(chalk_1.default.green('app.module.ts modificado😊') + ' Modificando AngularJson');
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.PrimeNGProcess.LerAngularJson:
            res.comando = res.nomeLocal + '/angular.json';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Lendo Angular.json'));
            fs_1.default.readFile(res.comando, (function (error, data) {
                var resultado = JSON.parse(data);
                var posicao = res.nomeDoApp;
                console.log(chalk_1.default.red('Modificando o Angular.json'));
                resultado.projects[posicao].architect.build.options.styles.push('src/PrimeFlex.scss');
                resultado.projects[posicao].architect.build.options.styles.push('node_modules/primeng/resources/themes/nova-light/theme.css');
                resultado.projects[posicao].architect.build.options.styles.push('node_modules/primeng/resources/primeng.min.css');
                resultado.projects[posicao].architect.build.options.styles.push('node_modules/primeicons/primeicons.css');
                temporizador = 0;
                config_1.config.Arquivos.PrimeNG.AngularJson = JSON.stringify(resultado);
                res.contador = config_1.config.PrimeNGProcess.ModificarAngularJson;
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.PrimeNGProcess.ModificarAngularJson:
            res.comando = res.nomeLocal + '/angular.json';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Salvando o novo Angular.json'));
            fs_1.default.writeFile(res.comando, config_1.config.Arquivos.PrimeNG.AngularJson, (function (err) {
                if (err !== null) {
                    rodarNPM.complete();
                    console.log(chalk_1.default.red('Error', err));
                }
                ;
                temporizador = 0;
                res.contador = config_1.config.PrimeNGProcess.criarPrimeFlex;
                console.log(chalk_1.default.green('angular.Json modificado😊') + ' criando PrimeFlex');
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.PrimeNGProcess.criarPrimeFlex:
            res.comando = res.nomeLocal + '/src/primeFlex.scss';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Criando o PrimeFlex pra ajustar no layout da Pagina'));
            fs_1.default.writeFile(res.comando, config_1.config.Arquivos.PrimeNG.PrimeFlex, (function (err) {
                if (err !== null) {
                    rodarNPM.complete();
                    console.log(chalk_1.default.red('Error', err));
                }
                temporizador = 0;
                tempo.unsubscribe();
                finalizarTimer.next();
                console.log(chalk_1.default.green('E pronto😊. Boa sorte'));
                rodarNPM.complete();
            }));
            break;
        default:
            break;
    }
});
