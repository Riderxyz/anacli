"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = __importDefault(require("chalk"));
var inquirer = require('inquirer');
var cmdBashTerminal = __importStar(require("child_process"));
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var ora_1 = __importDefault(require("ora"));
var config_1 = require("./../config");
var fs_1 = __importDefault(require("fs"));
var nebularquestions_1 = require("./nebularquestions");
var contador = 0;
var temporizador = 0;
var finalizarTimer = new rxjs_1.Subject();
var rodarNPM = new rxjs_1.Subject();
var spinner = ora_1.default(config_1.config.Processo.CriandoAngular);
exports.criarNebular = (function () {
    console.log('Okay, o que temos aqui hein senhores?');
    console.log('YES! Nebular ai vamos nós =>');
    inquirer.prompt(nebularquestions_1.questions).then(function (result) {
        console.log(result);
        console.log(chalk_1.default.magenta('Olá. Estou inicializando seu aplicativo agora'));
        spinner.start();
        console.log(chalk_1.default.blueBright(''));
        console.log(chalk_1.default.blueBright('AVISO! WARNING! HEY LINK, LISTEN!'));
        console.log(chalk_1.default.blueBright('AVISO! WARNING! HEY LINK, LISTEN!'));
        console.log(chalk_1.default.blueBright('AVISO! WARNING! HEY LINK, LISTEN!'));
        console.log(chalk_1.default.blueBright('AVISO! WARNING! HEY LINK, LISTEN!'));
        console.log(chalk_1.default.blueBright('Iniciando criação do Nebular'));
        var comando = 'ng new ' + result.AngularProjectName + ' --style=scss --routing';
        var res = {
            comando: comando,
            nomeLocal: './' + result.AngularProjectName + '',
            nomeDoApp: result.AngularProjectName,
            dataTransitorio: null,
            contador: config_1.config.PrimeNGProcess.CriarAngular
        };
        rodarNPM.next(res);
    });
});
rodarNPM.asObservable().subscribe(function (res) {
    var tempo = rxjs_1.timer(2000, 1000).pipe(operators_1.takeUntil(finalizarTimer))
        .subscribe(function (deltaT) {
        temporizador = deltaT;
        if (temporizador === 20) {
            console.log(chalk_1.default.yellow('Eu sei que ta demorando, mas espera só mais um pouquinho'));
        }
        if (temporizador === 40) {
            console.log(chalk_1.default.yellow('Okay, talvez eu esteja enrolando um pouco. Vou tentar acelar mais as coisas'));
        }
        if (temporizador === 60) {
            console.log(chalk_1.default.yellow('1 MINUTO! EBAAAaaa... é...eu sei'));
        }
        if (temporizador === 120) {
            console.log(chalk_1.default.yellow('Em minha defesa eu...peço desculpas 😔'));
        }
    });
    switch (res.contador) {
        case config_1.config.NebularProcess.CriarAngular:
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Criando angularApp'));
            cmdBashTerminal.exec(res.comando, (function (error, stdout, stderr) {
                spinner.stop();
                if (error !== null) {
                    console.log(chalk_1.default.red('Error', error));
                }
                temporizador = 0;
                console.log(chalk_1.default.green('Tudo funcionando'));
                console.log(chalk_1.default.green('Angular Criado. Configurando Ambiente PrimeNG'));
                spinner.start();
                res.contador = config_1.config.NebularProcess.RodarNpm;
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.NebularProcess.RodarNpm:
            res.comando = 'cd ' + res.nomeLocal + config_1.config.NPMInstances.NebularProcess;
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Instalando pacotes NPM'));
            cmdBashTerminal.exec(res.comando, (function (error, stdout, stderr) {
                spinner.stop();
                console.log(stdout);
                res.comando = '';
                temporizador = 0;
                res.contador = config_1.config.NebularProcess.CriarNebularModule;
                console.log(chalk_1.default.green('Nebular instalado. Vou criar um Modulo com alguns imports do Nebular para você.😊'));
                console.log(chalk_1.default.green('Se precisar de mais componentes Nebular, só ir no site da Akveo. coloco ele no final do modulo pra você'));
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.NebularProcess.CriarNebularModule:
            res.comando = res.nomeLocal + '/src/nebular.module.ts';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Criando Modulo do Nebular'));
            fs_1.default.writeFile(res.comando, config_1.config.Arquivos.Nebular.NebularModule, (function (err) {
                if (err !== null) {
                    console.log(chalk_1.default.red('Error', err));
                }
                temporizador = 0;
                res.contador = config_1.config.NebularProcess.ModificarAppModule;
                console.log(chalk_1.default.green('NebularModule.ts criado😊') + ' inserindo no app.module.ts');
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.NebularProcess.ModificarAppModule:
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Modificando o app.module.ts'));
            res.comando = res.nomeLocal + '/src/app/app.module.ts';
            fs_1.default.writeFile(res.comando, config_1.config.Arquivos.Nebular.AppModuleModificado, (function (err) {
                if (err !== null) {
                    console.log(chalk_1.default.red('Error', err));
                }
                temporizador = 0;
                res.contador = config_1.config.NebularProcess.LerAngularJson;
                console.log(chalk_1.default.green('app.module.ts modificado😊') + ' Modificando AngularJson');
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.NebularProcess.LerAngularJson:
            res.comando = res.nomeLocal + '/angular.json';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Lendo Angular.json'));
            fs_1.default.readFile(res.comando, (function (error, data) {
                var resultado = JSON.parse(data);
                var posicao = res.nomeDoApp;
                console.log(chalk_1.default.red('Modificando o Angular.json'));
                resultado.projects[posicao].architect.build.options.styles.push('src/PrimeFlex.scss');
                resultado.projects[posicao].architect.build.options.styles.push('node_modules/@nebular/theme/styles/prebuilt/cosmic.css');
                resultado.projects[posicao].architect.build.options.styles.push('node_modules/nebular-icons/scss/nebular-icons.scss');
                resultado.projects[posicao].architect.build.options.styles.push('node_modules/eva-icons/style/eva-icons.css');
                resultado.projects[posicao].architect.build.options.styles.push('node_modules/socicon/css/socicon.css');
                temporizador = 0;
                config_1.config.Arquivos.Nebular.AngularJson = JSON.stringify(resultado);
                res.contador = config_1.config.PrimeNGProcess.ModificarAngularJson;
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.NebularProcess.ModificarAngularJson:
            res.comando = res.nomeLocal + '/angular.json';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Salvando o novo Angular.json'));
            fs_1.default.writeFile(res.comando, config_1.config.Arquivos.Nebular.AngularJson, (function (err) {
                if (err !== null) {
                    console.log(chalk_1.default.red('Error', err));
                }
                temporizador = 0;
                res.contador = config_1.config.NebularProcess.criarPrimeFlex;
                console.log(chalk_1.default.green('angular.Json modificado😊') + ' criando PrimeFlex');
                rodarNPM.next(res);
            }));
            break;
        case config_1.config.NebularProcess.criarPrimeFlex:
            res.comando = res.nomeLocal + '/src/primeFlex.scss';
            console.log(chalk_1.default.red('comando a ser executado:', res.comando));
            console.log(chalk_1.default.red('Criando o PrimeFlex pra ajustar no layout da Pagina'));
            fs_1.default.writeFile(res.comando, config_1.config.Arquivos.Nebular.PrimeFlex, (function (err) {
                if (err !== null) {
                    console.log(chalk_1.default.red('Error', err));
                }
                temporizador = 0;
                tempo.unsubscribe();
                finalizarTimer.next();
                console.log(chalk_1.default.green('E pronto😊. Boa sorte'));
                rodarNPM.complete();
            }));
            break;
    }
});
// npm publish --registry http://10.2.1.127:4873
