export interface inquirer {
    type: 'input' | 'number' | 'confirm' | 'list' | 'rawlist' | 'expand' | 'checkbox' | 'password' | 'editor';
    name: string;
    message: string | Function;
    default: string | number | boolean | Array<any> | Function;
    choices?: Array<any> | Function;
    validate?: Function;
    filter?: Function;
    transformer?: Function;
    when?: Function | Boolean;
    pageSize?: number;
    prefix?: string;
    suffix?: string;
}
export declare let questions: inquirer[];
