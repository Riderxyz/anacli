"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.questions = [
    {
        type: 'input',
        name: 'AngularProjectName',
        message: 'Qual será o nome do Projeto?',
        default: 'kaleido',
        filter: (function (res) {
            return res.toLowerCase();
        })
    }
];
